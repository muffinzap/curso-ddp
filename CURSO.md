# Curso Drupal 9

## Drush

Instalación de drush:
```shell
composer require drush/drush
```

Comandos útiles:
 - `drush cr` : limpia la caché
 - `drush en nombre_modulo` : activa un módulo
 - `drusn pmu nombre_modulo` : desinstala un módulo
 - `drush sql:dump > bbdd.sql` : genera un backup de base de datos
 - `drush sqlc < bbdd.sql` : importa la bbdd esportada anteriormente
 - `drush cex` : exporta configuraciones
 - `drush cim` : importa configuraciones


## Módulos instalados
- admin_toolbar : barra de administración
- devel : opciones de desarrollo
- pathauto : patrones url
- webform : formularios
- webp: convierte imágenes a webp

Módulos del core:
- responsive images
- layout builder
