<?php

namespace Drupal\ddp_pages\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\UserInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Returns responses for Ddp pages routes.
 */
class DdpPagesController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function calculator($num1, $num2) {

    if (!is_numeric($num1) || !is_numeric($num2)) {
      throw new BadRequestHttpException(t('No numeric arguments specified.'));
    }

    $list[] = $this->t("@num1 + @num2 = @sum",
      [
        '@num1' => $num1,
        '@num2' => $num2,
        '@sum' => $num1 + $num2
      ]
    );
    $list[] = $this->t("@num1 - @num2 = @difference",
      [
        '@num1' => $num1,
        '@num2' => $num2,
        '@difference' => $num1 - $num2
      ]
    );
    $list[] = $this->t("@num1 x @num2 = @product",
      [
        '@num1' => $num1,
        '@num2' => $num2,
        '@product' => $num1 * $num2
      ]
    );

    if ($num2 != 0)
      $list[] = $this->t("@num1 / @num2 = @division",
        [
          '@num1' => $num1,
          '@num2' => $num2,
          '@division' => $num1 / $num2
        ]
      );
    else {
      $list[] = $this->t("@num1 / @num2 = undefined (division by zero)",
        array('@num1' => $num1, '@num2' => $num2));
    }

    $output['galicloud_pages_calculator'] = [
      '#title' => $this->t('Operations:'),
      '#theme' => 'item_list',
      '#items' => $list,
    ];

    $list[] = $this->t("Username: @username", ['@username' => $user->getAccountName()]);
    $list[] = $this->t("Email: @email", ['@email' => $user->getEmail()]);
    $list[] = $this->t("Roles: @roles", ['@roles' => implode(', ', $user->getRoles())]);
    $list[] = $this->t("Last accessed time: @lastaccess", array('@lastaccess' => \Drupal::service('date.formatter')->format($user->getLastAccessedTime(), 'short')));
    $output['galicloud_pages_user'] = [
      '#theme' => 'item_list',
      '#items' => $list,
      '#title' => $this->t('User data:'),
    ];
    return $output;

  }

  /**
   * Builds the response.
   */
  public function user(UserInterface $user)
  {

    $list[] = $this->t("Username: @username", ['@username' => $user->getAccountName()]);
    $list[] = $this->t("Email: @email", ['@email' => $user->getEmail()]);
    $list[] = $this->t("Roles: @roles", ['@roles' => implode(', ', $user->getRoles())]);
    $list[] = $this->t("Last accessed time: @lastaccess", array('@lastaccess' => \Drupal::service('date.formatter')->format($user->getLastAccessedTime(), 'short')));
    $output['galicloud_pages_user'] = [
      '#theme' => 'item_list',
      '#items' => $list,
      '#title' => $this->t('User data:'),
    ];
    return $output;

  }


}
