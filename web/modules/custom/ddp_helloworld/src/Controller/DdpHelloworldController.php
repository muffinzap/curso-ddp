<?php

namespace Drupal\ddp_helloworld\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for ddp_helloworld routes.
 */
class DdpHelloworldController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $currentUser = \Drupal::currentUser();

    $name = 'Mundo';
    if (!$currentUser->getAccount()->isAnonymous()) {
      $name = $currentUser->getAccount()->getAccountName();
    }

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('Hello @name, how are you?', ['@name' => $name]),
      '#title' => $this->t('Hello world title'),
    ];

    $items = ["UNO", "DOS"];

    $build['other'] = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];



    return $build;
  }

}
